#include "pvp.h"
#include "mtrx.c"
#include "utils.c"
#include "graphic.c"
#include "network.c"
#include "mechanics.c"
#include "mesh.h"

int main (int argc, char **argv) {
    Screen screen;
    Renderer geometry_render, shadow_render, depthmap_render;
    Camera cam = {}, sun = {};
    Model player_model[MAX_PEERS], terrain_model, depthmap_model;
    Model *models_ptr[MAX_PEERS+2] = { &player_model[0], &terrain_model }, *depthmodelptr = &depthmap_model; // @DEBUG
    //const uint8_t *keystate = SDL_GetKeyboardState(NULL);
    Timer timer = {};
    //Mtrx projm, viewm;
    //Quat qtx, qty, qtz;

//restore_session_jump: // not implemented :: use in fullscreen window
    // global buffer
    gbuffer = alloc_buffer(1408, 64);
    assertq(gbuffer.data != NULL);
    restore_session(&player_model[0]);
    
    create_opengl_context(&screen, /*SHOW_FPS |*/ VSYNC_ON /*| FULLSCREEN_ON |*/, 400, 300);
    network_init(false);
    timer.now = timer.last_frame = timer.last_sec = SDL_GetTicks();
    // geometry renderer
    geometry_render.viewport = (Viewport){ .w = screen.display.w, .h = screen.display.h };
    program_from_shader_source(&geometry_render, GEOMETRY_RENDERER, GLDIR"geometry.vsh", GLDIR"geometry.fsh");
    retrieve_attributes(&geometry_render, "vertex", "fst_edge", "snd_edge");
    retrieve_uniforms(&geometry_render, 5, "mtrx", "vect", "shadow_map", "shininess", "wabble"); // MAX_UNIFORMS = 5
    geometry_render.clear_flags = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT;
    // shadow renderer
    GLsizei fbsize = sqrtf(screen.display.w * screen.display.h); 
    shadow_render.viewport = (Viewport){ .w = fbsize, .h = fbsize };
    program_from_shader_source(&shadow_render, SHADOW_RENDERER, GLDIR"shadow.vsh", GLDIR"shadow.fsh");
    retrieve_framebuffer(&shadow_render);
    retrieve_attributes(&shadow_render, "vertex", NULL, NULL);
    retrieve_uniforms(&shadow_render, 1, "mtrx");
    shadow_render.clear_flags = GL_DEPTH_BUFFER_BIT;
    // depth map renderer
    depthmap_render.viewport = (Viewport){ screen.display.w - fbsize/4, screen.display.h - fbsize/4, fbsize/4, fbsize/4 };
    program_from_shader_source(&depthmap_render, DEPTHMAP_RENDERER, GLDIR"depthmap.vsh", GLDIR"depthmap.fsh");
    retrieve_attributes(&depthmap_render, "vertex", "mapcoord", NULL);
    retrieve_uniforms(&depthmap_render, 1, "depthmap");
    depthmap_render.clear_flags = 0;
    // player model
    retrieve_buffer_objects(&player_model[0], &player_mesh, 2, geometry_render.program_index, shadow_render.program_index);
    vertex_attribute_data(&player_model[0], &geometry_render);
    vertex_attribute_data(&player_model[0], &shadow_render);
    player_model[0].color = (Vect){ 1.0f, 0.8f, 0.7f };
    player_model[0].shininess = 32.0f;
    // terrain model
    retrieve_buffer_objects(&terrain_model, &terrain_mesh, 1, geometry_render.program_index);
    vertex_attribute_data(&terrain_model, &geometry_render);
    vertex_attribute_data(&terrain_model, &shadow_render);
    terrain_model.color = (Vect){ 0.5f, 0.4f, 0.35f };
    terrain_model.shininess = 16.0f;
    // depth map
    retrieve_buffer_objects(&depthmap_model, (Mesh *)&depthmap_mesh, 1, depthmap_render.program_index);
    vertex_attribute_data(&depthmap_model, &depthmap_render);
    // @create a new index array for each player

    set_opengl_options();

    mtrx_memory_set(4*sizeof(Mtrx), &gbuffer.data); // 2^8 = 256 = 4 matrices
    //Mtrx id_mtrx = (Mtrx){ .rx = 1.0f, .uy = 1.0f, .fz = 1.0f, .tw = 1.0f };
    //Quat id_quat = (Quat){ 0.0f, 0.0f, 0.0f, 1.0f };

    //network_init();
    //test_broadcast_and_hairpin();
    sun_camera(&sun, &screen);
    cam_camera(&cam, &screen);
    reach_server(&timer);
    gflag &= ~RESTORE_SESSION;

    while (~gflag & QUIT) {
        update_timer(&timer);
        if (gflag & SERVER_ANSWERED) {
            get_reports(player_model, models_ptr, &timer);
        } else {
            answer_from_server(player_model, models_ptr, &timer);
        }
        read_input(player_model, &cam, &sun, &screen);
        cam.viewm = *view_mtrx(&cam.eye, &cam.aim, &cam.up); // @DEBUG
        sun.viewm = *view_mtrx(&sun.eye, &sun.aim, &sun.up); // @DEBUG
        sun.projm = *ortho_mtrx(kb, kc, kd, kd); // @DEBUG
        move_player(&player_model[0], &timer);
        draw_models(&shadow_render, &sun, 0, num_peers, models_ptr);
        draw_models(&geometry_render, &cam, shadow_render.framebuffer, num_peers+1, models_ptr, &sun);
        draw_models(&depthmap_render, NULL, shadow_render.framebuffer, 1, &depthmodelptr);
        //printf("%f %f\n", sun.eye.z, sun.eye.x);
        //printf("%.1f, %.1f, %.1f\n", kb, kc, kd);
        if (gflag & SERVER_ANSWERED)
            inform_peers(player_model, models_ptr, &timer);
        SDL_GL_SwapWindow(screen.window);
        // @CHANGE terrain: maximux triangle edges' length = 1.0f
    }
    screen_close(&screen, &player_model[0]);
    return 0;
}

