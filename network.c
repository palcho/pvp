/*******************************************
 *  REFACTOR THE CODE TO BE MORE READABLE  *
 *******************************************/
#define MAX_PEERS 16
#define SERVER_HOST "54.94.187.24"
#define SERVER_PORT 54321
#define LOCAL_HOST "0.0.0.0"
#define LOCAL_PORT 54321
#define TRY_REACHING_AGAIN_COOLDOWN 400 // 400ms
#define KEEP_ALIVE_COOLDOWN 2000 // 2s
#define SCAN_PEERS_COOLDOWN 500 // 500 ms
#define UNRESPONSIVINESS_THRESHOLD 60000 // 1min
#define LAG_THRESHOLD 800 // ms
#define STRIP(x) ((uint8_t *)(x))[0], ((uint8_t *)(x))[1], ((uint8_t *)(x))[2], ((uint8_t *)(x))[3]
#define HAS_MET_PEER 0x0001

// structs prototypes
typedef struct Peer_      Peer;
typedef struct Formulary_ Formulary;
typedef struct Report_    Report;

// functions prototypes
uint16_t random_port (void);
int find_address (IPaddress *address, Peer *ptr, int num);
int find_equivalent_peer (Peer *peer, Peer *ptr, int num);
int network_init (int enable_printnet, ...); // if enabled: ... = char *printnet_listener.host, uint16_t printnet_listener.port
int printn (const char *format, ...);
int reach_server (Timer *timer);
int answer_from_server (Model *model, Model **models_ptr, Timer *timer);
void inform_peers (Model *model, Model **models_ptr, Timer *timer);
void get_reports (Model *model, Model **models_ptr, Timer *timer);
void restore_session (Model *model);
void screen_close (Screen *screen, Model *model);
void remove_from_peers (int peer_index, Model *model, Model **models_ptr);
void remove_from_newcomers (int newcomer_index);

enum msg_header {
    BROADCAST_TEST           = 0x9e057b21,
    NEW_PLAYER               = 0xfa3b54c8, // or new public port
    NEW_PLAYER_ACK           = 0xa21b59fe,
    RECEIVE_PEERS            = 0x0f42c152,
    HAIRPIN_TEST             = 0x54d1c8fa,
    HAIRPIN_RESULT           = 0x6721d4a9,
    HAIRPIN_RESULT_ACK       = 0x26781543,
    PEER_REPORT              = 0x862c5b9e,
    KEEP_ALIVE               = 0x1c57e906,
    UNRECOGNIZED             = 0xa9b25341,
    PEER_UNRESPONSIVE        = 0xe304b2c3, // @may remove peer from server, or move to other world, or split world (in case of hairpin issues)
    PEER_RESPONSIVE          = 0x965c4e28,
    PEER_RESPONSIVINESS_ACK  = 0x324cdb10,
    PEER_UNMET               = 0x24151897,
    PEER_MET                 = 0x1895f1f2,
    PEER_METTINESS_ACK       = 0x5a6213d5,
    PEER_BACKGROUNDING       = 0xf2631c04,
    LEAVING_PLAYER           = 0x1572d1fb
};

#define arrive_time public.arrv_time
#define last_seen public.arrv_time
#define ack_flags private.ack
#define acks_remaining private.ack
struct Peer_ {
    union {
        IPaddress address;
        struct { uint32_t host; uint16_t port; int16_t arrv_time; };
    } public;
    union {
        IPaddress address;
        struct { uint32_t host; uint16_t port; int16_t ack; };
    } private;
};
struct Formulary_ {
    uint32_t header;
    Peer peer;
};
struct Report_ {
    uint32_t header;
    uint32_t time;
    Vect color;
    Vect translation;
    Quat orientation;
    Vect dr, ds;
};

static UDPsocket socket = NULL;
static UDPpacket packet, printnet_packet;
static Peer *local, server, peer[MAX_PEERS], newcomer[16];

// public addresses have different ports, because of NAT
int num_peers = 1, num_unacked = 0;

int network_init (int enable_printnet, ...) { // if enabled: ... = char *printnet_listener.host, uint16_t printnet_listener.port
    int status;
    uint16_t port;
    Formulary *form = (Formulary *)gbuffer.data;
    local = &peer[0];
    // initialization
    socket = NULL; num_peers = 1; num_unacked = 0;
    status = SDLNet_Init();
    asserth(status != -1, network_init_error);
    // open udp socket with random port
    if (~gflag & RESTORE_SESSION) {
        port = random_port();
        for (int i = 0; socket == NULL && i < 10; i++) {
            port = 49152 + ((port + 0xb391) & 0x3fff);
            socket = SDLNet_UDP_Open(port);
        }
    } else {
        port = local->private.port;
        socket = SDLNet_UDP_Open(port);
    }
    asserth(socket != NULL, network_init_error);
    // server address
    status = SDLNet_ResolveHost(&server.public.address, SERVER_HOST, SERVER_PORT);
    asserth(status != -1, network_init_resolve_host_error);
    // broadcast message to find local ip address
    packet = (UDPpacket) {
        .address = (IPaddress){ INADDR_BROADCAST, port },
        .data = (uint8_t *)gbuffer.data,
        .len = sizeof(uint32_t),
        .maxlen = gbuffer.size,
        .channel = -1
    };
    SDLNet_Write16(port, &packet.address.port);
    SDLNet_Write32(BROADCAST_TEST, &form->header);
    status = SDLNet_UDP_Send(socket, -1, &packet);
    status ? (gflag |= ALLOW_BROADCAST) : (gflag &= ~ALLOW_BROADCAST);
    // @remember to retrieve message and then reach server
    if (enable_printnet) {
        char *listener_host;
        uint16_t listener_port;
        gflag |= ENABLE_PRINTNET;
        va_list printnet_list;
        va_start(printnet_list, enable_printnet);
        listener_host = va_arg(printnet_list, char *);
        listener_port = va_arg(printnet_list, int);
        va_end(printnet_list);
        status = SDLNet_ResolveHost(&printnet_packet.address, listener_host, listener_port);
        asserth(status != -1, network_init_resolve_host_error);
        printnet_packet.maxlen = 140;
        printnet_packet.channel = -1;
    }
    printf(gflag & ALLOW_BROADCAST ? "broadcast sent to find private address\n" : "broadcasts are not allowed in this subnet: private address will be unknown\n");
    return 1;

network_init_resolve_host_error:
    SDLNet_UDP_Close(socket);
    socket = NULL;
network_init_error:
    return 0;
}

int printn (const char *format, ...) {
    char buffer[140];
    int status = 1;
    va_list ap;
    va_start(ap, format);
    if (gflag & ENABLE_PRINTNET) {
        packet.len = vsnprintf(buffer, printnet_packet.maxlen, format, ap);
        packet.data = (uint8_t *)buffer;
        status = SDLNet_UDP_Send(socket, -1, &packet);
    } else {
        packet.len = vprintf(format, ap);
    }
    va_end(ap);
    return status * packet.len;
}

int reach_server (Timer *timer) {
    int status = 1;
    Formulary *form = (Formulary *)gbuffer.data;
    if (gflag & ALLOW_BROADCAST) {
        // get local private address
        packet.data = (uint8_t *)gbuffer.data;
        status = SDLNet_UDP_Recv(socket, &packet);
        if (status == 1 && SDLNet_Read32(packet.data) == BROADCAST_TEST) {
            local->private.address = packet.address;
            printf("private address = %u.%u.%u.%u:%"PRIu16"\n", STRIP(&packet.address.host), SDLNet_Read16(&packet.address.port));
        } else {
            printf("broadcast did not return\n");
        }
    }
    if (status <= 0) return 0;
    // actually reach server with local's private address (if broadcast's allowed)
    SDLNet_Write32(NEW_PLAYER, &form->header);
    form->peer.private.address = local->private.address;
    packet.data = (uint8_t *)form;
    packet.len = sizeof(Formulary);
    packet.address = server.public.address;
    timer->last_reached_server = timer->now;
    status = SDLNet_UDP_Send(socket, -1, &packet);
    printf("now trying to reach server\n");
    printf("0x%04x 0x%04x 0x%04x 0x%04x\n", local->private.port, SDL_SwapBE16(local->private.port), SDL_SwapBE16(SDL_SwapBE16(local->private.port)), SDL_Swap16(local->private.port));
    return status;
}

// get public and peers' adresses from server
// @make a while loop to get messages !!!!!
// @send keep_alive messages to the server
// @echo general interest messages from server to peers
int answer_from_server (Model *model, Model **models_ptr, Timer *timer) {
    Formulary *form = (Formulary *)packet.data;
    packet.data = (uint8_t *)gbuffer.data;
    int answer;
    do {
        answer = SDLNet_UDP_Recv(socket, &packet);
    } while (answer > 0 && form->header != SDL_SwapBE32(RECEIVE_PEERS));
    if (answer <= 0) { // no answer yet
        if (timer->now - timer->last_reached_server > TRY_REACHING_AGAIN_COOLDOWN) {
            // reach server again
            SDLNet_Write32(NEW_PLAYER, &form->header);
            form->peer.private.address = local->private.address;
            packet.data = (uint8_t *)form;
            packet.len = sizeof(Formulary);
            packet.address = server.public.address;
            timer->last_reached_server = timer->now;
            SDLNet_UDP_Send(socket, -1, &packet);
            printf("trying again to reach server\n");
        }
        return 0;
    } else if (packet.address.host == server.public.host) { // server answered!
        int length = (packet.len - sizeof(Formulary)) / sizeof(Peer);
        Peer *received_peer = (Peer *)&form[1];
        if (SDLNet_Read32(&form->header) == RECEIVE_PEERS) { // answered indeed!
            // store local public ip
            local->public.address = form->peer.public.address;
            // send hairpin probe
            packet.address = local->public.address;
            printf("sent hairpin probe to %u.%u.%u.%u:%"PRIu16"\n", STRIP(&packet.address.host), SDLNet_Read16(&packet.address.port)); // @DEBUG
            SDLNet_Write32(HAIRPIN_TEST, &form->header);
            packet.len = sizeof(uint32_t);
            SDLNet_UDP_Send(socket, -1, &packet);
            // store peers
            Model *terrain_model = models_ptr[1];
            for (int i = 0, j = 1; i < length; i++) {
                if (local->public.host == received_peer[i].public.host && local->public.port == received_peer[i].public.port)
                    continue;
                peer[j] = received_peer[i];
                peer[j].last_seen = timer->now & ~HAS_MET_PEER;
                peer[j].ack_flags = 0;
                model[j] = model[0];
                model[j].translation = (Vect){ 0.0f, 2.0f*(j), 0.0f };
                models_ptr[j] = &model[j];
                j++;
                printf("peer: public{%u.%u.%u.%u:%"PRIu16"},private{%u.%u.%u.%u:%"PRIu16"}\n", STRIP(&received_peer[i].public.address.host), SDLNet_Read16(&received_peer[i].public.port), STRIP(&received_peer[i].private.address.host), SDLNet_Read16(&received_peer[i].private.port)); // @DEBUG
            }
            models_ptr[length] = terrain_model;
            num_peers = length;
            printf("SERVER ANSWERED: number of peers = %d\n", num_peers);
            gflag |= SERVER_ANSWERED;
            return num_peers;
        }
        return 0;
    }
    return 0;
}

void inform_peers (Model *model, Model **models_ptr, Timer *timer) {
    packet.data = (uint8_t *)gbuffer.data;
    packet.len = sizeof(Report);
    Report *report = (Report *)packet.data;
    // fill report with network byte order
    SDLNet_Write32(PEER_REPORT, &report->header);
    SDLNet_Write32(timer->now, &report->time);
    for (int i = 0; i < sizeof(Quat)/sizeof(float); i++)
        report->orientation.q[i] = SDL_SwapFloatBE(model->orientation.q[i]);
    for (int i = 0; i < sizeof(Vect)/sizeof(float); i++) {
        report->translation.v[i] = SDL_SwapFloatBE(model->translation.v[i]);
        report->dr.v[i] = SDL_SwapFloatBE(model->dr.v[i]);
        report->ds.v[i] = SDL_SwapFloatBE(model->ds.v[i]);
        report->color.v[i] = SDL_SwapFloatBE(model->color.v[i]);
    }
    // @IMPLEMENT: deal with unbroadcastables and unhaipininables within lan
    for (int i = 1; i < num_peers; i++) {
        packet.address = (peer[i].public.host != local->public.host) ? peer[i].public.address : peer[i].private.address;
        SDLNet_UDP_Send(socket, -1, &packet);
        //printf("sent message to %u.%u.%u.%u:%"PRIu16"\n", STRIP(&packet.address.host), SDLNet_Read16(&packet.address.port));
    }
    // @make its own memory to keep record of 8 previous packets sent
    // send keep-alive messages to server
    if (timer->now - timer->last_reached_server >= KEEP_ALIVE_COOLDOWN) {
        Formulary *form = (Formulary *)packet.data;
        timer->last_reached_server = timer->now;
        SDLNet_Write32(KEEP_ALIVE, &form->header);
        form->peer = *local;
        packet.len = sizeof(Formulary);
        packet.address = server.public.address;
        SDLNet_UDP_Send(socket, -1, &packet);
    }
    if (timer->now - timer->last_scan >= SCAN_PEERS_COOLDOWN) {
        Formulary *form = (Formulary *)gbuffer.data;
        timer->last_scan = timer->now;
        SDLNet_Write32(NEW_PLAYER, &form->header);
        packet.len = sizeof(Formulary);
        uint16_t time_diff;
        for (int i = 0; i < num_unacked; i++) {
            // remove fully acknowledged newcomer from newcomer list and its corresponding flags
            if (!newcomer[i].acks_remaining) {
                remove_from_newcomers(i);
                continue;
            }
            // resend new players's message to those who didn't acknowledged it
            time_diff = (timer->now & 0xffff) - newcomer[i].arrive_time;
            if (time_diff >= SCAN_PEERS_COOLDOWN) {
                form->peer = newcomer[i];
                for (int j = 1; j < num_peers; j++) {
                    if (~peer[j].ack_flags & (1 << i)) {
                        packet.address = (peer[j].public.host != local->public.host) ? peer[j].public.address : peer[j].private.address;
                        SDLNet_UDP_Send(socket, -1, &packet);
                    }
                }
            }
        }// @merge messages to the same peer into one
        // check for unresponsive peers and alert others and server if there is one
        // @NEED to be as rigid as NEW_PLAYER (answer with REPONSIVE or UNRESPONSIVE so that server sorts the situation)
        for (int i = 1; i < num_peers; i++) {
            time_diff = (timer->now & 0xffff) - (peer[i].last_seen & 0xfffe);
            if (time_diff > UNRESPONSIVINESS_THRESHOLD) {
                int newcomer_index = find_address(&peer[i].public.address, newcomer, num_unacked);
                printf("%u.%u.%u.%u:%"PRIu16" unresponsive\n", STRIP(&peer[i].public.host), SDLNet_Read16(&peer[i].public.port));
                SDLNet_Write32((peer[i].last_seen & HAS_MET_PEER) ? PEER_UNRESPONSIVE : PEER_UNMET, &form->header); // @NEED a weaker state of unresponsiviness before expelling
                form->peer = peer[i];
                for (int j = 1; j < num_peers; j++) {
                    packet.address = (peer[j].public.host != local->public.host) ? peer[j].public.address : peer[j].private.address;
                    SDLNet_UDP_Send(socket, -1, &packet);
                }
                packet.address = server.public.address;
                SDLNet_UDP_Send(socket, -1, &packet);
                remove_from_peers(i, model, models_ptr);
                remove_from_newcomers(newcomer_index);
            }
        }
    }
}

void get_reports (Model *model, Model **models_ptr, Timer *timer) {
    Formulary *form = (Formulary *)gbuffer.data;
    Report *report = (Report *)gbuffer.data;
    packet.data = (uint8_t *)gbuffer.data;
    int sender_index, newcomer_index, peer_index;
    while (SDLNet_UDP_Recv(socket, &packet) > 0) {
        switch (SDLNet_Read32(&form->header)) {
            case NEW_PLAYER: // @check for server's seal
                printf("NEW_PLAYER (from %u.%u.%u.%u:%"PRIu16"): public{%u.%u.%u.%u:%"PRIu16"},private{%u.%u.%u.%u:%"PRIu16"}\n", STRIP(&packet.address.host), SDLNet_Read16(&packet.address.port), STRIP(&form->peer.public.host), SDLNet_Read16(&form->peer.public.port), STRIP(&form->peer.private.host), SDLNet_Read16(&form->peer.private.port)); // @DEBUG
                if (find_address(&form->peer.public.address, peer, num_peers) < 0) {
                    peer_index = find_equivalent_peer(&form->peer, peer, num_peers); // @check peer's signature
                    if (peer_index < 0) { // a true newcomer
                        // add in addresses list
                        peer_index = num_peers++;
                        peer[peer_index] = form->peer;
                        // add in models list
                        model[peer_index] = model[0];
                        model[peer_index].translation = (Vect){ 0.0f, 2.0f*peer_index, 0.0f };
                        // push terrain model forward on draw order
                        models_ptr[num_peers] = models_ptr[num_peers-1];
                        // add in models pointer draw order list
                        models_ptr[peer_index] = &model[peer_index];
                    } else { // NAT changed peer's public port
                        peer[peer_index].public.port = form->peer.public.port;
                    }
                    // peer's time keeping and flags
                    peer[peer_index].last_seen = timer->now & ~HAS_MET_PEER;
                    peer[peer_index].ack_flags = (1 << (num_unacked+1)) - 1;
                    // add to newcomers quarentine to confirm peers know about them
                    newcomer[num_unacked] = form->peer;
                    newcomer[num_unacked].arrive_time = timer->now;
                    newcomer[num_unacked].acks_remaining = num_peers - 2; // minus the newcomer and I
                    printf("Bflag %d: 0x%04"PRIx16", rm[%d] = %"PRIu16"\n", peer_index, peer[peer_index].ack_flags, num_unacked, newcomer[num_unacked].acks_remaining);
                    // open space in peers' flags to newcomer
                    /* // @testing if it isn't necessary
                    for (int i = 1; i < num_peers; i++)
                        peer[i].ack_flags <<= 1;
                    */
                    // mark peer as acknowledger of new player if it's not the server
                    if (packet.address.host != server.public.host) {
                        newcomer[num_unacked].acks_remaining--;
                        int index = find_address(&packet.address, peer, num_peers);
                        if (index >= 0) {
                            peer[index].ack_flags |= 1 << num_unacked;
                            printf("Cflag %d: 0x%04"PRIx16", rm[%d] = %"PRIu16"\n", index, peer[index].ack_flags, num_unacked, newcomer[num_unacked].acks_remaining);
                        }
                    }
                    // alert other peers about newcomer
                    IPaddress sender_address = packet.address;
                    // @IMPLEMENT: deal with unbroadcastables and unhaipininables within lan
                    for (int i = 1; i < num_peers-1; i++) {
                        if (i == peer_index) continue;
                        packet.address = (peer[i].public.host != local->public.host) ? peer[i].public.address : peer[i].private.address;
                        SDLNet_UDP_Send(socket, -1, &packet);
                    }
                    // and the server, if the first message wasn't from him
                    if (sender_address.host != server.public.host) {
                        SDLNet_Write32(NEW_PLAYER_ACK, &form->header);
                        packet.address = server.public.address;
                        SDLNet_UDP_Send(socket, -1, &packet);
                    }
                    packet.address = sender_address;
                    num_unacked++;
                } else { // address already received
                    sender_index = find_address(&packet.address, peer, num_peers);
                    newcomer_index = find_address(&form->peer.public.address, newcomer, num_unacked);
                    // register reception
                    if (sender_index >=0 && newcomer_index >= 0) {
                        if (~peer[sender_index].ack_flags & (1 << newcomer_index)) {
                            peer[sender_index].ack_flags |= (1 << newcomer_index);
                            newcomer[newcomer_index].acks_remaining--;
                            printf("Dflag %d: 0x%04"PRIx16", rm[%d] = %"PRIu16"\n", sender_index, peer[sender_index].ack_flags, newcomer_index, newcomer[newcomer_index].acks_remaining);
                        }
                    }
                }
                // acknowledge new player to sender
                SDLNet_Write32(NEW_PLAYER_ACK, &form->header);
                packet.len = sizeof(Formulary);
                SDLNet_UDP_Send(socket, -1, &packet);
                break;
            case NEW_PLAYER_ACK:
                printf("NEW_PLAYER_ACK (from %u.%u.%u.%u:%"PRIu16"): public{%u.%u.%u.%u:%"PRIu16"},private{%u.%u.%u.%u:%"PRIu16"}\n", STRIP(&packet.address.host), SDLNet_Read16(&packet.address.port), STRIP(&form->peer.public.host), SDLNet_Read16(&form->peer.public.port), STRIP(&form->peer.private.host), SDLNet_Read16(&form->peer.private.port)); // @DEBUG
                sender_index = find_address(&packet.address, peer, num_peers);
                if (sender_index >= 0) { // address already received
                    newcomer_index = find_address(&form->peer.public.address, newcomer, num_unacked);
                    // register reception
                    if (sender_index >=0 && newcomer_index >= 0) {
                        if (~peer[sender_index].ack_flags & (1 << newcomer_index)) {
                            peer[sender_index].ack_flags |= (1 << newcomer_index);
                            newcomer[newcomer_index].acks_remaining--;
                            printf("Eflag %d: 0x%04"PRIx16", rm[%d] = %"PRIu16"\n", sender_index, peer[sender_index].ack_flags, newcomer_index, newcomer[newcomer_index].acks_remaining);
                        }
                    }
                }
                break;
            case HAIRPIN_TEST: 
                printf("hairpin test succeeded\n");
                if (packet.address.host == local->public.host && packet.address.port == local->public.port) {
                    packet.address = server.public.address;
                    SDLNet_Write32(HAIRPIN_RESULT, &form->header);
                    packet.len = sizeof(uint32_t);
                    SDLNet_UDP_Send(socket, -1, &packet);
                }
                break;
            case PEER_REPORT:
                    //printf("PEER_REPORT from %u.%u.%u.%u:%"PRIu16"\n", STRIP(&packet.address.host), SDLNet_Read16(&packet.address.port));
                peer_index = find_address(&packet.address, peer, num_peers);
                if (peer_index >= 0) {
                    peer[peer_index].last_seen = timer->now | HAS_MET_PEER;
                    // fill model with host byte order
                    for (int i = 0; i < sizeof(Quat)/sizeof(float); i++) {
                        model[peer_index].orientation.q[i] = SDL_SwapFloatBE(report->orientation.q[i]);
                    }
                    for (int i = 0; i < sizeof(Vect)/sizeof(float); i++) {
                        model[peer_index].translation.v[i] = SDL_SwapFloatBE(report->translation.v[i]);
                        model[peer_index].dr.v[i] = SDL_SwapFloatBE(report->dr.v[i]);
                        model[peer_index].ds.v[i] = SDL_SwapFloatBE(report->ds.v[i]);
                        model[peer_index].color.v[i] = SDL_SwapFloatBE(report->color.v[i]);
                    }
                }
                break;
            case UNRECOGNIZED:
                if (packet.address.host == server.public.host) {
                    printf("SERVER KNOWS US NOT!\n");
                    models_ptr[1] = models_ptr[num_peers];
                    num_peers = 1;
                    num_unacked = 0;
                    gflag &= ~SERVER_ANSWERED;
                    timer->last_reached_server -= TRY_REACHING_AGAIN_COOLDOWN;
                }
                break;
            case PEER_UNRESPONSIVE:
            case PEER_RESPONSIVE:
            case PEER_UNMET:
                break; //@IMPLEMENT
            case LEAVING_PLAYER:
                //@check for certificate signature
                peer_index = find_address(&form->peer.public.address, peer, num_peers);
                if (peer_index >= 0) { // @need to free vaos vbos ebos etc
                    printf("LEAVING_PLAYER: %u.%u.%u.%u:%"PRIu16", %u.%u.%u.%u:%"PRIu16"\n",\
                        STRIP(&form->peer.public.host), SDLNet_Read16(&form->peer.public.port), STRIP(&form->peer.private.host), SDLNet_Read16(&form->peer.private.port));
                    newcomer_index = find_address(&form->peer.public.address, newcomer, num_unacked);
                    remove_from_peers(peer_index, model, models_ptr);
                    remove_from_newcomers(newcomer_index);
                    // alert server and peers
                    packet.address = server.public.address;
                    SDLNet_UDP_Send(socket, -1, &packet);
                    for (int i = 1; i < num_peers; i++) {
                        packet.address = (peer[i].public.host != local->public.host) ? peer[i].public.address : peer[i].private.address;
                        SDLNet_UDP_Send(socket, -1, &packet);
                    }
                }
                break;
            case PEER_BACKGROUNDING:
                printf("PEER_BACKGROUNDING: %u.%u.%u.%u:%"PRIu16", %u.%u.%u.%u:%"PRIu16"\n", STRIP(&form->peer.public.host), SDLNet_Read16(&form->peer.public.port), STRIP(&form->peer.private.host), SDLNet_Read16(&form->peer.private.port));
                break;
            default:
                printf("GIBBERISH from %u.%u.%u.%u:%"PRIu16"\n", STRIP(&packet.address.host), SDLNet_Read16(&packet.address.port));
                break;
        }
    } // no reports -> wabbling
    uint16_t time_diff;
    for (int i = 1; i < num_peers; i++) {
        time_diff = (timer->now & 0xffff) - (peer[i].last_seen & 0xfffe);
        model[i].wabble = (time_diff > LAG_THRESHOLD) ? fmodf(timer->now * 0.005f + (float)i, 2.0f*PI) : 0.0f;
        //printf("wabble %d = %.6f\n", i, model[i].wabble);
    }
}

int find_address (IPaddress *address, Peer *ptr, int num) {
    int found = 0, i;
    for (i = 0; i < num; i++) {
        if (ptr[i].public.host == address->host) {
            if (ptr[i].public.port == address->port) {
                found = 1; break;
            }
        } else if (ptr[i].private.host == address->host) {
            if (ptr[i].private.port == address->port) {
                found = 1; break;
            }
        }
    }
    return found ? i : -1;
}

int find_equivalent_peer (Peer *peer, Peer *ptr, int num) {
    for (int i = 0; i < num; i++) {
        if (ptr[i].public.host == peer->public.host \
        &&  ptr[i].private.host == peer->private.host \
        &&  ptr[i].private.port == peer->private.port)
            return i;
    }
    return -1;
}


uint16_t random_port (void) {
    uint16_t port = 0x51f8;
    uint16_t *ptr = (uint16_t *)&gbuffer.unaligned_ptr;
    for (int i = 0; i < sizeof(ptr)/2; i++) {
        port ^= ptr[i];
    }
    /*
    time_t timing = time(NULL);
    for (int i = 0; i < sizeof(time_t)/2; i++)
        port ^= ((uint16_t *)timing)[i];
    */
    return port;
}

void screen_close (Screen *screen, Model *model) {
    Report *restore = (Report *)gbuffer.data;
    //@sign a certificate
    SDL_GL_DeleteContext(screen->context);
    gflag &= ~OPENGL_INITIALIZED;
    Formulary *form = (Formulary *)gbuffer.data;
    SDLNet_Write32(gflag & BACKGROUND ? PEER_BACKGROUNDING : LEAVING_PLAYER, &form->header);
    form->peer = *local;
    packet.len = sizeof(Formulary);
    packet.address = server.public.address;
    SDLNet_UDP_Send(socket, -1, &packet);
    for (int i = 1; i < num_peers; i++) {
        packet.address = (peer[i].public.host != local->public.host) ? peer[i].public.address : peer[i].private.address;
        SDLNet_UDP_Send(socket, -1, &packet);
    }
    if (gflag & BACKGROUND) {
        *restore = (Report) {
            .header = SDLNet_Read16(&local->private.port),
            .time = time(NULL),
            .color = model->color,
            .orientation = model->orientation,
            .translation = model->translation,
            .dr = model->dr,
            .ds = model->ds
        };
        write_file(restore, sizeof(Report), SDL_RWFromFile("restore.pvp", "w"));
    }
    free(gbuffer.unaligned_ptr);
    SDLNet_UDP_Close(socket);
    SDLNet_Quit();
    SDL_DestroyWindow(screen->window);
    SDL_Quit();
}

/*****************************************DO THIS********************************************/
void remove_from_newcomers (int newcomer_index) {
    if (newcomer_index < 0) return;
    for (int peer_index = 1; peer_index < num_peers; peer_index++) {
        peer[peer_index].ack_flags = ((peer[peer_index].ack_flags >> 1) & ~((1 << newcomer_index) - 1)) | (peer[peer_index].ack_flags & ((1 << newcomer_index) - 1));
        printf("Aflag %d: 0x%04"PRIx16", rm[%d] = %"PRIu16"\n", peer_index, peer[peer_index].ack_flags, newcomer_index, newcomer[newcomer_index].acks_remaining);
    }
    newcomer[newcomer_index--] = newcomer[--num_unacked];
}

void remove_from_peers (int peer_index, Model *model, Model **models_ptr) {
    if (peer_index < 0) return;
    // acknowlege unacknowledged peers from leaving peer
    for (int newcomer_index = 0; newcomer_index < num_unacked; newcomer_index++) {
        if (~peer[peer_index].ack_flags & (1 << newcomer_index))
            newcomer[newcomer_index].acks_remaining--;
    }
    // remove from addresses list
    peer[peer_index] = peer[num_peers-1];
    // remove from models list
    model[peer_index] = model[num_peers-1];
    // place terrain model in the right place
    models_ptr[num_peers-1] = models_ptr[num_peers];
    num_peers--;
}
/*****************************************UNTIL THIS****************************************/
    

void restore_session (Model *model) {
    Report *restore = (Report *)gbuffer.data;
    size_t nread;
    nread = read_file(&gbuffer, SDL_RWFromFile("restore.pvp", "r"));
    if (nread == sizeof(Report) && time(NULL) - restore->time < UNRESPONSIVINESS_THRESHOLD/1000) {
        gflag = RESTORE_SESSION;
        peer[0].private.port = restore->header;
        model->orientation = restore->orientation;
        model->translation = restore->translation;
        model->dr = restore->dr;
        model->ds = restore->ds;
        model->wabble = PI;
    } else {
        gflag = 0;
        model->orientation = (Quat){ 0.0f, 0.0f, 0.0f, 1.0f };
        model->translation = (Vect){ 0.0f, 0.0f, 0.0f };
        model->dr = model->ds = (Vect){ 0.0f, 0.0f, 0.0f };
    }
    write_file(restore, 0, SDL_RWFromFile("restore.pvp", "w"));
}


#undef arrive_time
#undef last_seen
#undef ack_flags
#undef acks_remaining

