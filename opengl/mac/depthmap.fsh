#version 410

in vec2 map_coord;
uniform sampler2D depthmap;

out vec4 Frag_Color;

void main (void) {
    FragColor = texture(depthmap, map_coord);
}
