#version 410

in vec3  diffuse;
in vec3 _normal;
in vec3  proj_coord;
in vec3  reflect_dir;
in vec3  frag_pos;
in vec3  obj_color;
in vec3  view_pos;
in float bias;

uniform sampler2D shadow_map;
uniform float shininess;

out vec4 Frag_Color;

void main (void) {

    vec3 light_color = vec3(1.0, 1.0, 1.0);
    vec3 ambient = 0.05 * light_color;

    // specular lighting
    vec3 view_dir = normalize(view_pos - frag_pos);
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0), shininess);
    //float spec = dot(view_dir, reflect_dir) > 0.0 ? 1.0 : 0.0 * shininess;
    vec3 specular = spec * light_color;

    // shadow snd Phong lighting model
    // get closest depth value from light's perspective
    float closest_depth = texture(shadow_map, proj_coord.xy).r;
    // get depth of current fragment from light's perspective
    float current_depth = proj_coord.z;
    float shadow = (current_depth - bias) > closest_depth ? 1.0 : 0.0;

    // Phong lighting model
    vec3 result = (ambient + (1.0 - 0.8*shadow) * diffuse + (1.0 - shadow) * specular) * obj_color;

    FragColor = vec4(result, 1.0);
}

