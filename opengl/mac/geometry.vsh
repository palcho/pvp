#version 410

in vec3 vertex;
in vec4 fst_edge;
in vec4 snd_edge;

uniform mat4 mtrx[4];
uniform vec3 vect[3];
uniform float wabble;

out vec3 diffuse;
out vec3 normal;
out vec3 proj_coord;
out vec3 reflect_dir;
out vec3 frag_pos;
out vec3 obj_color;
out vec3 view_pos;
out float bias;

#define vertex_model    mtrx[0]
#define vtx_fullmtrx    mtrx[1]
#define normal_model    mtrx[2]
#define lightspace_mtrx mtrx[3]
#define light_dir       vect[0]
#define object_color    vect[1]
#define _view_pos       vect[2]
#define PI 3.14159265

void main (void) {
    vec3 vtx = vertex;
    normal = normalize(cross(fst_edge.xyz, snd_edge.xyz));
    if (wabble > 0.0) { // triangle and normal wabbling
        vec3 v1 = vertex + fst_edge.xyz + normal * 0.01*sin(wabble + fst_edge.w * PI + PI/3.0);
        vec3 v2 = vertex + snd_edge.xyz + normal * 0.01*sin(wabble + fst_edge.w * PI - PI/3.0);
        vtx += normal * 0.01*sin(wabble + fst_edge.w * PI);
        normal = normalize(cross(v1 - vtx, v2 - vtx));
    }
    normal = mat3(normal_model) * normal;

/*
    vec4 vtx = vec4(vertex, 1.0);
    if (wabble > 0.0) // triangle wabbling
        vtx.xyz += normal.xyz * 0.01*sin(wabble + normal.w * 3.14159265);
    _normal = mat3(normal_model) * normal.xyz;
*/
    frag_pos = vec3(vertex_model * vec4(vtx, 1.0));
    vec4 fragpos_lightspace = lightspace_mtrx * vec4(frag_pos, 1.0);

    vec3 light_color = vec3(1.0, 1.0, 1.0);
    if (dot(normal, _view_pos - frag_pos) < 0.0) // @DEBUG
        normal = -normal;
    float diff = max(dot(normal, light_dir), 0.0);
    diffuse = diff * light_color;

    reflect_dir = reflect(-light_dir, normal);

    proj_coord = fragpos_lightspace.xyz / fragpos_lightspace.w;
    proj_coord = proj_coord * 0.5 + 0.5;
    bias = max(0.005 * (1.0 - dot(normal, light_dir)), 0.0005);
    obj_color = object_color;
    view_pos = _view_pos;

    gl_Position = vtx_fullmtrx * vec4(vtx, 1.0);
}

