#version 410

in vec2 vertex;
in vec2 mapcoord;
out   vec2 map_coord;

void main (void) {
    map_coord = mapcoord;
    gl_Position = vec4(vertex, 0.0, 1.0);
}
