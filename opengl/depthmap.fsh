#version 120

varying vec2 map_coord;
uniform sampler2D depthmap;

void main (void) {
    gl_FragColor = texture2D(depthmap, map_coord);
}
