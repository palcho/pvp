#version 120

attribute vec3 vertex;

uniform mat4 mtrx;

void main (void) {
    gl_Position = mtrx * vec4(vertex, 1.0);
}
