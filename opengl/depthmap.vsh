#version 120

attribute vec2 vertex;
attribute vec2 mapcoord;
varying   vec2 map_coord;

void main (void) {
    map_coord = mapcoord;
    gl_Position = vec4(vertex, 0.0, 1.0);
}
