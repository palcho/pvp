#define DELTA_ROTATION 0.001f
#define DELTA_STRAIGHT 0.001f
#define MOUSE_MOVE_ADJUST 0.1f
#define TOUCH_MOVE_ADJUST 10.0f
#define TOUCH_ROTATE_ADJUST 40.0f
#define NTFID 8547403
float kb = 1.0f, kc = 30.0f, kd = 10.0f; // @DEBUG

typedef struct TouchFinger_  TouchFinger;
typedef union  TouchControl_ TouchControl;

struct TouchFinger_ {
    SDL_FingerID fingerId;
    struct { float x, y; } delta;
};
union TouchControl_ {
    struct { TouchFinger left, right; };
};

enum movement_flags {
    MOVE_FORWARD  = 0x01,
    MOVE_BACKWARD = 0x02,
    MOVE_LEFT     = 0x04,
    MOVE_RIGHT    = 0x08,
    TURN_LEFT     = 0x10,
    TURN_RIGHT    = 0x20,
    LOOK_UP       = 0x40,
    LOOK_DOWN     = 0x80,
    USING_MOUSE   = 0x0100
};
enum side { LEFT, RIGHT };

void read_input (Model *model, Camera *cam, Camera *sun, Screen *screen) {
    static uint16_t mflag = 0;
    static TouchControl touch_ctrl = {};
    SDL_Event event;
    if (mflag & USING_MOUSE) {
        model->dr = cam->dr = (Vect){ 0.0f, 0.0f, 0.0f };
    }
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                gflag |= BACKGROUND;
                gflag |= QUIT; break; // @opengl expansion robustness
            case SDL_FINGERDOWN: // @store finger id and touch position, and sort by side to know if it moves camera or character
                if (event.tfinger.x < 0.5f) {
                    if (touch_ctrl.left.fingerId != NTFID) continue;
                    touch_ctrl.left = (TouchFinger){ event.tfinger.fingerId, 0, 0 };
                } else {
                    if (touch_ctrl.right.fingerId != NTFID) continue;
                    touch_ctrl.right = (TouchFinger){ event.tfinger.fingerId, 0, 0 };
                } break;
            case SDL_FINGERUP: // @remove finger id 
                if (touch_ctrl.left.fingerId == event.tfinger.fingerId) {
                    touch_ctrl.left = (TouchFinger){ .fingerId = NTFID };
                    model->ds.x = model->ds.y = model->ds.z = 0;
                } else if (touch_ctrl.right.fingerId == event.tfinger.fingerId) {
                    touch_ctrl.right = (TouchFinger){ .fingerId = NTFID };
                    model->dr.x = model->dr.y = model->dr.z = 0;
                } break;
            case SDL_FINGERMOTION: // @check finger id to move character or camera
                if (touch_ctrl.left.fingerId == event.tfinger.fingerId) {
                    touch_ctrl.left.delta.x += event.tfinger.dx / screen->sizef.h;
                    touch_ctrl.left.delta.y += event.tfinger.dy / screen->sizef.w;
                    model->ds.x = touch_ctrl.left.delta.x * TOUCH_MOVE_ADJUST;
                    model->ds.z = -touch_ctrl.left.delta.y * TOUCH_MOVE_ADJUST;
                } else if (touch_ctrl.right.fingerId == event.tfinger.fingerId) {
                    touch_ctrl.right.delta.x += event.tfinger.dx / screen->sizef.w;
                    touch_ctrl.right.delta.y += event.tfinger.dy / screen->sizef.h;
                    model->dr.y = -touch_ctrl.right.delta.x * TOUCH_ROTATE_ADJUST;
                    model->dr.x = touch_ctrl.right.delta.y * TOUCH_ROTATE_ADJUST;
                } break;
            case SDL_MOUSEBUTTONDOWN:// @check button to enter attack or defense instance
            case SDL_MOUSEBUTTONUP: // @get out of instance or attack depending of instance and direction moved by the mouse during instance mode
            case SDL_MOUSEMOTION: // @move camera or, if in a instance, choose attack or defense direction
                mflag |= USING_MOUSE;
                model->dr.x = event.motion.yrel / screen->sizef.h * MOUSE_MOVE_ADJUST;
                model->dr.y = -event.motion.xrel / screen->sizef.w * MOUSE_MOVE_ADJUST;
                break;

            case SDL_KEYDOWN:
                if (event.key.repeat) continue;
                switch (event.key.keysym.sym) {
                    case SDLK_AC_BOOKMARKS:
                    case SDLK_AC_FORWARD:
                    case SDLK_AC_HOME:
                    case SDLK_AC_REFRESH:
                    case SDLK_AC_SEARCH:
                    case SDLK_AC_STOP:
                        gflag |= BACKGROUND;
                    case SDLK_AC_BACK:
                    case SDLK_ESCAPE:
                        gflag |= QUIT; break;
                    case SDLK_LEFT: //@DEBUG
                        cam->eye.x -= 0.1f; break;
                    case SDLK_RIGHT: //@DEBUG
                        cam->eye.x += 0.1f; break;
                    case SDLK_UP: //@DEBUG
                        cam->eye.z += 0.1f; break;
                    case SDLK_DOWN: //@DEBUG
                        cam->eye.z -= 0.1f; break;
                    case SDLK_a: //@DEBUG
                        mflag |= MOVE_LEFT;
                        model->ds.x = -DELTA_STRAIGHT;
                        cam->ds.x = -DELTA_STRAIGHT;
                        break;
                    case SDLK_d: //@DEBUG
                        mflag |= MOVE_RIGHT;
                        model->ds.x = DELTA_STRAIGHT;
                        cam->ds.x = DELTA_STRAIGHT;
                        break;
                    case SDLK_w: //@DEBUG
                        mflag |= MOVE_FORWARD;
                        model->ds.z = DELTA_STRAIGHT;
                        cam->ds.z = DELTA_STRAIGHT;
                        break;
                    case SDLK_s: //@DEBUG
                        mflag |= MOVE_BACKWARD;
                        model->ds.z = -DELTA_STRAIGHT;
                        cam->ds.z = -DELTA_STRAIGHT;
                        break;
                    default: break;
                }
                break;
            case SDL_KEYUP:
                switch (event.key.keysym.sym) {
                    case SDLK_a:
                        mflag &= ~MOVE_LEFT;
                        model->ds.x = (mflag & MOVE_RIGHT) ? DELTA_STRAIGHT : 0.0f;
                        cam->ds.x = (mflag & MOVE_RIGHT) ? DELTA_STRAIGHT : 0.0f;
                        break;
                    case SDLK_d:
                        mflag &= ~MOVE_RIGHT;
                        model->ds.x = (mflag & MOVE_LEFT) ? -DELTA_STRAIGHT : 0.0f;
                        cam->ds.x = (mflag & MOVE_LEFT) ? -DELTA_STRAIGHT : 0.0f;
                        break;
                    case SDLK_w:
                        mflag &= ~MOVE_FORWARD;
                        model->ds.z = (mflag & MOVE_BACKWARD) ? -DELTA_STRAIGHT : 0.0f;
                        cam->ds.z = (mflag & MOVE_BACKWARD) ? -DELTA_STRAIGHT : 0.0f;
                        break;
                    case SDLK_s:
                        mflag &= ~MOVE_BACKWARD;
                        model->ds.z = (mflag & MOVE_FORWARD) ? DELTA_STRAIGHT : 0.0f;
                        cam->ds.z = (mflag & MOVE_FORWARD) ? DELTA_STRAIGHT : 0.0f;
                        break;
                    default: break;
                }
                break;
            default: break;
        }
    }
}

void sun_camera (Camera *cam, Screen *screen) {
    cam->eye = (Vect){ 1.0f, 4.0f, 2.0f };
    cam->aim = (Vect){ 0.0f, 0.0f, 0.0f };
    cam->up  = (Vect){ 0.0f, 1.0f, 0.00001f };
    cam->dr = cam->ds = (Vect){};
    cam->viewm = *view_mtrx(&cam->eye, &cam->aim, &cam->up);
    cam->projm = *ortho_mtrx(1.0f, 30.0f, 10.0f, 10.0f);
};

void cam_camera (Camera *cam, Screen *screen) {
    cam->eye = (Vect){ 0.36f, 2.00f, -1.94f };
    cam->aim = (Vect){ 0.34f, 1.62f, 1.01f };
    cam->up  = (Vect){ 0.0f, 1.0f, 0.0f };
    cam->dr = cam->ds = (Vect){};
    cam->viewm = *view_mtrx(&cam->eye, &cam->aim, &cam->up);
    cam->projm = *persp_mtrx(0.1f, 100.0f, PI/3.0f, screen->sizef.w/screen->sizef.h);
}

void move_player  (Model *model, Timer *timer) {
    Vect up    = { 0.0f, 1.0f,  0.0f };
    Vect front = { 0.0f, 0.0f, -1.0f };
    Vect right;
    // rotate
    model->orientation = *mul_qq(quat_from_vect(&up, model->dr.y * timer->dt), &model->orientation);
    // translate
    front = *mul_qviq(&model->orientation, &front);
    right = *cross(&front, &up);
    model->translation = *add_vv(&model->translation, add_vv(mul_sv(model->ds.x * timer->dt, &right), mul_sv(model->ds.z * timer->dt, &front)));
}

