REQUIRE = $(wildcard *.c *.h opengl/glad.*)
SRC = pvp.c opengl/glad.c
CHANGE_FROM_LINUX = -e 's:^//\(\#define PLATFORM\):\1:' -e 's:^\#define LINUX://\0:'
CHANGE_TO_LINUX = -e 's:^//\(\#define LINUX\):\1:' -e 's:^\#define PLATFORM://\0:'
RELEASE_VERSION = -e 's:^\#define DEBUG_VERSION://\0:'
DEBUG_VERSION = -e 's:^//\(\#define DEBUG_VERSION\):\1:'
CFLAGS = -std=c99 -Wall -Wno-missing-braces -lm -lSDL2 -lSDL2_net
DFLAGS = -Og -ggdb
RFLAGS = -O3
LFLAGS = -I$(HOME)/fcGL/include -L$(HOME)/fcGL/lib
BLENDIR = $(HOME)/blender/meshes/pvp
ANDROID_ASSETS_DIR = $(HOME)/AndroidProjects/pvp/app/src/main/assets
ANDROID_SRC_DIR = $(HOME)/AndroidProjects/pvp/app/src/main/jni/src
ANDROID_FILES = $(addprefix $(ANDROID_SRC_DIR)/, $(wildcard *.c *.h opengles/glad.*)) $(addprefix $(ANDROID_ASSETS_DIR)/, $(wildcard opengles/*sh))
ANDROID_REQUIRE = $(REQUIRE) $(wildcard opengles/*sh)
WINDOWS_FLAGS = -I$(HOME)/android_lib/x86_64-w64-mingw32/include -L$(HOME)/android_lib/x86_64-w64-mingw32/lib -lmingw32 -lSDL2main
MESHES_HEADERS = $(BLENDIR)/player.h $(BLENDIR)/terrain.h $(BLENDIR)/depthmap.h
.PHONY: clean android_def

# makefile targets
debug: pvpdb
cin: cinpvpdb
asm: pvp.s
linux: pvp
windows: pvp.exe
android: $(ANDROID_FILES)
shaders: $(addprefix opengl/mac/,$(notdir $(wildcard opengles/*sh)))

# build targets
pvpdb: $(REQUIRE)
	gcc $(SRC) $(CFLAGS) $(DFLAGS) -o $@

cinpvpdb: $(filter-out mesh.h,$(REQUIRE))
	gcc $(SRC) $(LFLAGS) $(CFLAGS) $(DFLAGS) -o $@
	export LD_LIBRARY_PATH=$(HOME)/fcGL/lib

pvp.s: $(REQUIRE)
	sed -i $(RELEASE_VERSION) platform.def
	gcc $(SRC) $(CFLAGS) $(RFLAGS) -S
	sed -i $(DEBUG_VERSION) platform.def

pvp: $(REQUIRE)
	sed -i $(RELEASE_VERSION) platform.def
	gcc $(SRC) $(CFLAGS) $(RFLAGS) -o $@
	sed -i $(DEBUG_VERSION) platform.def

pvp.exe: $(REQUIRE)
	sed -i $(subst PLATFORM,WINDOWS,$(CHANGE_FROM_LINUX)) $(RELEASE_VERSION) platform.def
	x86_64-w64-mingw32-gcc $(SRC) $(WINDOWS_FLAGS) $(CFLAGS) $(RFLAGS) -o $@
	sed -i $(subst PLATFORM,WINDOWS,$(CHANGE_TO_LINUX)) $(DEBUG_VERSION) platform.def

# copy android files
$(ANDROID_SRC_DIR)/%.c: %.c
	cp $< $(dir $@)
$(ANDROID_SRC_DIR)/%.h: %.h
	cp $< $(dir $@)
$(ANDROID_SRC_DIR)/opengles/%.h: opengles/%.h
	cp $< $(dir $@)
$(ANDROID_SRC_DIR)/opengles/%.c: opengles/%.c
	cp $< $(dir $@)
$(ANDROID_ASSETS_DIR)/opengles/%.vsh: opengles/%.vsh
	cp $< $(dir $@)
$(ANDROID_ASSETS_DIR)/opengles/%.fsh: opengles/%.fsh
	cp $< $(dir $@)

# make meshes
mesh.h: $(MESHES_HEADERS)
	cat $(MESHES_HEADERS) > mesh.h

$(BLENDIR)/%.h: $(BLENDIR)/%.obj $(BLENDIR)/mkmesh
	$(BLENDIR)/mkmesh $< $@

$(BLENDIR)/mkmesh: $(BLENDIR)/mkmesh.c
	gcc $(BLENDIR)/mkmesh.c -o $(BLENDIR)/mkmesh

# shaders' automatic version adaptation
opengl/%.vsh: opengles/%.vsh
	sed -r -e '1 s/^(\#version) 100/\1 120/' -e 's/\<(high|medium|low)p\> *//g' < $< > $@
opengl/%.fsh: opengles/%.fsh
	sed -r -e '1 s/^(\#version) 100/\1 120/' -e 's/\<(high|medium|low)p\> *//g' < $< > $@
opengl/mac/%.vsh: opengl/%.vsh
	sed -r -e '1 s/^(\#version) 120/\1 410/' -e 's/\<attribute\> */in /' -e 's/\<varying\> */out /' < $< > $@
opengl/mac/%.fsh: opengl/%.fsh
	sed -r -e '1 s/^(\#version) 120/\1 410/' -e 's/\<varying\> */in /' -e '/^void main/ i\out vec4 Frag_Color;\n' -e 's/\<(texture)2D\>/\1/g' -e 's/\<gl_(FragColor)\>/\1/' < $< > $@

# cleaning
clean:
	-rm -f pvp pvpdb cinpvpdb *.s *.exe restore.pvp

# update platform.def for android
android_def:
	sed $(subst PLATFORM,ANDROID,$(CHANGE_FROM_LINUX)) $(RELEASE_VERSION) < platform.def > $(ANDROID_SRC_DIR)/platform.def
