#version 100

attribute highp   vec2 vertex;
attribute mediump vec2 mapcoord;
varying   mediump vec2 map_coord;

void main (void) {
    map_coord = mapcoord;
    gl_Position = vec4(vertex, 0.0, 1.0);
}
