#version 100

attribute highp vec3 vertex;

uniform highp mat4 mtrx;

void main (void) {
    gl_Position = mtrx * vec4(vertex, 1.0);
}
