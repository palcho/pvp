#version 100

varying mediump vec2 map_coord;
uniform mediump sampler2D depthmap;

void main (void) {
    gl_FragColor = texture2D(depthmap, map_coord);
}
