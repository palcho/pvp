#version 100

varying lowp    vec3  diffuse;
varying mediump vec3 _normal;
varying mediump vec3  proj_coord;
varying mediump vec3  reflect_dir;
varying mediump vec3  frag_pos;
varying lowp    vec3  obj_color;
varying mediump vec3  view_pos;
varying lowp    float bias;

uniform sampler2D shadow_map;
uniform lowp float shininess;

void main (void) {

    lowp vec3 light_color = vec3(1.0, 1.0, 1.0);
    lowp vec3 ambient = 0.05 * light_color;

    // specular lighting
    mediump vec3 view_dir = normalize(view_pos - frag_pos);
    lowp float spec = pow(max(dot(view_dir, reflect_dir), 0.0), shininess);
    //lowp float spec = dot(view_dir, reflect_dir) > 0.0 ? 1.0 : 0.0 * shininess;
    lowp vec3 specular = spec * light_color;

    // shadow snd Phong lighting model
    // get closest depth value from light's perspective
    mediump float closest_depth = texture2D(shadow_map, proj_coord.xy).r;
    // get depth of current fragment from light's perspective
    mediump float current_depth = proj_coord.z;
    lowp float shadow = (current_depth - bias) > closest_depth ? 1.0 : 0.0;

    // Phong lighting model
    lowp vec3 result = (ambient + (1.0 - 0.8*shadow) * diffuse + (1.0 - shadow) * specular) * obj_color;

    gl_FragColor = vec4(result, 1.0);
}

