#ifndef FCGR_INCLUDE_HEADER
#define FCGR_INCLUDE_HEADER

// platform and debug definitions
#include "platform.def"

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>
#include <string.h>
#include <stddef.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#if defined(WINDOWS) || defined(LINUX)
#   include <SDL2/SDL.h>
#   include "opengl/glad.h"
#   include <SDL2/SDL_net.h>
#elif defined(MAC)
#   include <SDL2/SDL.h>
#   include "opengl/glad.h"
#   include <SDL2_net/SDL_net.h>
#elif defined(ANDROID) || defined(IOS)
#   include <SDL.h>
#   include <opengles/glad.h>
#   include <SDL_net.h>
#endif

#if defined(LINUX) || defined(WINDOWS)
#   define GLDIR "opengl/"
#elif defined(MAC)
#   define GLDIR "opengl/mac/"
#elif defined(ANDROID) || defined(IOS)
#   define GLDIR "opengles/"
#endif

#endif // FCGR_INCLUDE_HEADER
