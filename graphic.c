#define MAX_ATTRIBUTES 2
#define MAX_UNIFORMS 5
#define MAX_PROGRAMS 3

// structs prototypes
typedef struct Screen_    Screen;
typedef struct Vertex_    Vertex;
typedef struct Index_     Index;
typedef struct Mesh_      Mesh;
typedef struct Model_     Model;
typedef struct Renderer_  Renderer;
typedef struct Viewport_  Viewport;
typedef struct Camera_    Camera;
typedef struct MapMesh_   MapMesh;
typedef struct MapVertex_ MapVertex;

// function prototypes
void create_opengl_context (Screen *screen, uint32_t flags, ...); // ... = width, height (if not FULLCREEN_ON)
void program_from_shader_source (Renderer *renderer, int program_index, const char *vertsh_file, const char *fragsh_file);
void retrieve_attributes (Renderer *renderer, const char *vertex_attrib_name, const char *fst_edge_attrib_name, const char *snd_edge_attrib_name);
void retrieve_uniforms (Renderer *renderer, int count, ...); // ... = name of uniforms
void retrieve_buffer_objects (Model *model, const Mesh *mesh, int num_vaos, ...); // ... = program indices
void vertex_attribute_data (const Model *model, const Renderer *renderer);
void retrieve_framebuffer (Renderer *renderer);
void draw_models (Renderer *renderer, Camera *camera, GLuint texture, int num_models, Model **model, ...); // Camera *light_source (for geometry renderer)
GLuint shader_from_sourcefile (GLenum type, const char *path);

struct Viewport_ {
    GLint x, y;
    GLsizei w, h;
};
struct Screen_ {
    SDL_DisplayMode display;
    SDL_GLContext context;
    SDL_Window *window;
    struct { float w, h; } sizef;
};
// different kinds of vertices data will arrive
struct Vertex_ {
    struct { float vx, vy, vz; };
    struct { int8_t nx, ny, nz, nw; };
    struct { int16_t rx, ry, rz, rw; };
    struct { int16_t sx, xy, sz, sw; };
};
struct Index_ {
    GLushort a, b, c;
};
// so different kinds of meshes shall appear too
struct Mesh_ {
    struct { Vertex *data; size_t size; } vertices;
    struct { Index  *data; size_t size; } indices;
};
struct MapMesh_ {
    struct { MapVertex *data; size_t size; } vertices;
    struct { Index *data; size_t size; } indices;
};
struct Model_ {
    Quat orientation;
    Vect translation;
    Vect dr, ds;
    Vect color;
    union { Mesh mesh; MapMesh map; };
    float shininess;
    float wabble; // = time.now if wabbling, = -1.0f if not
    GLuint vbo, ebo;
    GLuint vao[MAX_PROGRAMS];
};
struct Camera_ {
    Mtrx projm, viewm;
    Vect eye, aim, up;
    Vect dr, ds;
};
struct Renderer_ {
    int program_index;
    GLuint program, vertex_shader, fragment_shader, framebuffer, fbo;
#if defined(ANDROID) || defined(IOS)
    GLuint rbo; // for shadowmap inactive colorbuffer, which is necessary in OpenGLES
#endif
    struct { GLint vertex; union { GLint mapcoord; GLint fst_edge; }; GLint snd_edge; } attribute;
    Viewport viewport;
    GLbitfield clear_flags;
    GLint uniform[MAX_UNIFORMS];
};
struct MapVertex_ {
    struct { float vx, vy; }; // vertex
    struct { float cx, cy; }; // coordinate
};

enum program_index { GEOMETRY_RENDERER, SHADOW_RENDERER, DEPTHMAP_RENDERER };

void create_opengl_context (Screen *screen, uint32_t flags, ...) {
    assertq(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) >= 0);
#if defined(WINDOWS) || defined(LINUX) || defined(MAC)
    gflag = HAS_TERMINAL | (gflag & RESTORE_SESSION);
    assertq(SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE) >= 0);
    assertq(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2) >= 0);
    assertq(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1) >= 0);
    SDL_SetRelativeMouseMode(SDL_TRUE);
#elif defined(ANDROID) || defined(IOS)
    gflag = FULLSCREEN_ON | (gflag & RESTORE_SESSION);
    assertq(SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES) >= 0);
    assertq(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2) >= 0);
    assertq(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0) >= 0);
#endif
    gflag |= flags;
    assertq(SDL_GetCurrentDisplayMode(0, &screen->display) >= 0);
    if (~gflag & FULLSCREEN_ON) {
        va_list screen_size;
        va_start(screen_size, flags);
        screen->display.w = va_arg(screen_size, int);
        screen->display.h = va_arg(screen_size, int);
        va_end(screen_size);
    }
    printf("display: %dx%d\nrefresh rate = %d Hz\n", screen->display.w, screen->display.h, screen->display.refresh_rate);
    screen->sizef.w = (float)screen->display.w;
    screen->sizef.h = (float)screen->display.h;
    screen->window = SDL_CreateWindow("pvp", 0, 0, screen->display.w, screen->display.h,\
        SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI | (gflag & FULLSCREEN_ON ? SDL_WINDOW_FULLSCREEN : 0));
    assertq(screen->window != NULL);
    screen->context = SDL_GL_CreateContext(screen->window);
    assertq(screen->context != 0);
#if defined(WINDOWS) || defined(LINUX) || defined(MAC)
    gladLoadGLLoader(SDL_GL_GetProcAddress);
    assertq(GLAD_GL_ARB_framebuffer_object || (GLAD_GL_EXT_framebuffer_object && GLAD_GL_EXT_packed_depth_stencil) || GLAD_GL_VERSION_3_0);
    if (GLAD_GL_ARB_vertex_array_object || GLAD_GL_VERSION_3_0)
        gflag |= HAS_VAO;
    printf("%d %d %d %d\n", GLAD_GL_VERSION_3_0, GLAD_GL_ARB_vertex_array_object, GLAD_GL_ARB_framebuffer_object, GLAD_GL_EXT_framebuffer_object);
#elif defined(ANDROID) || defined(IOS)
    gladLoadGLES2Loader(SDL_GL_GetProcAddress);
    assertq((GLAD_GL_OES_depth_texture && GLAD_GL_OES_packed_depth_stencil) || GLAD_GL_ES_VERSION_3_0);
    if (GLAD_GL_OES_vertex_array_object || GLAD_GL_ES_VERSION_3_0)
        gflag |= HAS_VAO;
    printf("%d %d %d\n", GLAD_GL_ES_VERSION_3_0, GLAD_GL_OES_vertex_array_object, GLAD_GL_OES_depth_texture);
#endif
    gflag |= OPENGL_INITIALIZED;
    printf("vendor: %s\nrenderer: %s\nversion: %s\n",\
        glGetString(GL_VENDOR), glGetString(GL_RENDERER), glGetString(GL_VERSION));
    SDL_GL_SetSwapInterval(gflag & VSYNC_ON ? 1 : 0);
#ifdef ANDROID
    SDL_SetHint(SDL_HINT_ANDROID_SEPARATE_MOUSE_AND_TOUCH, "1");
#endif
}

void program_from_shader_source (Renderer *renderer, int program_index, const char *vertsh_file, const char *fragsh_file) {
    GLint status;
    renderer->program_index = program_index;
    renderer->program = glCreateProgram();
    assertq(renderer->program);
    renderer->vertex_shader = shader_from_sourcefile(GL_VERTEX_SHADER, vertsh_file);
    renderer->fragment_shader = shader_from_sourcefile(GL_FRAGMENT_SHADER, fragsh_file);
    assertq(renderer->vertex_shader && renderer->fragment_shader);

    GL(glAttachShader(renderer->program, renderer->vertex_shader));
    GL(glAttachShader(renderer->program, renderer->fragment_shader));
    GL(glLinkProgram(renderer->program));
    GL(glDetachShader(renderer->program, renderer->vertex_shader));
    GL(glDetachShader(renderer->program, renderer->fragment_shader));
    GL(glDeleteShader(renderer->vertex_shader));
    GL(glDeleteShader(renderer->fragment_shader));

    GL(glGetProgramiv(renderer->program, GL_LINK_STATUS, &status));
    assertq(status == GL_TRUE);

    renderer->framebuffer = renderer->fbo = 0;
#if defined(ANDROID) || defined(IOS)
    renderer->rbo = 0;
#endif
}

void retrieve_attributes (Renderer *renderer, const char *vertex_attrib_name, const char *fst_edge_attrib_name, const char *snd_edge_attrib_name) {
    renderer->attribute.vertex = glGetAttribLocation(renderer->program, vertex_attrib_name);
    assertq(renderer->attribute.vertex >= 0);
    if (snd_edge_attrib_name != NULL) { // normals will be used
        renderer->attribute.fst_edge = glGetAttribLocation(renderer->program, fst_edge_attrib_name);
        renderer->attribute.snd_edge = glGetAttribLocation(renderer->program, snd_edge_attrib_name);
        assertq(renderer->attribute.fst_edge >= 0 && renderer->attribute.snd_edge >= 0);
    } else if (fst_edge_attrib_name != NULL) { // texture coordinates will be used
        renderer->attribute.mapcoord = glGetAttribLocation(renderer->program, fst_edge_attrib_name);
        renderer->attribute.snd_edge = -1;
    } else {
        renderer->attribute.fst_edge = -1;
        renderer->attribute.snd_edge = -1;
    }
}

void retrieve_uniforms (Renderer *renderer, int count, ...) {
    GL(glUseProgram(renderer->program));
    va_list uniform_names;
    va_start(uniform_names, count);
    for (int i = 0; i < count; i++) {
        renderer->uniform[i] = glGetUniformLocation(renderer->program, va_arg(uniform_names, const char *));
        assertq(renderer->uniform[i] >= 0);
    }
    va_end(uniform_names);
    for (int i = count; i < MAX_UNIFORMS; i++)
        renderer->uniform[i] = -1;
}

void retrieve_buffer_objects (Model *model, const Mesh *mesh, int num_vaos, ...) { // cast to (Mesh *) if it's not a mesh
    for (int i = 0; i < MAX_PROGRAMS; i++)
        model->vao[i] = 0;
    va_list program_indices;
    va_start(program_indices, num_vaos);
    for (int i = 0; i < num_vaos; i++) {
        if (gflag & HAS_VAO) {
            GL(glGenVertexArrays(1, &model->vao[va_arg(program_indices, int)]));
        } else { // mark as user of that program
            model->vao[va_arg(program_indices, int)] = 1;
        }
    }
    va_end(program_indices);
    // vertex array data
    GL(glGenBuffers(1, &model->vbo));
    GL(glBindBuffer(GL_ARRAY_BUFFER, model->vbo));
    GL(glBufferData(GL_ARRAY_BUFFER, mesh->vertices.size, mesh->vertices.data, GL_STATIC_DRAW));
    // element array data, if used
    if (mesh->indices.data != NULL) {
        GL(glGenBuffers(1, &model->ebo));
        GL(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model->ebo));
        GL(glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->indices.size, mesh->indices.data, GL_DYNAMIC_DRAW));
    } // identity matrix
    if (~gflag & RESTORE_SESSION || model->wabble != PI) {
        model->orientation = (Quat){ 0.0f, 0.0f, 0.0f, 1.0f };
        model->translation = (Vect){ 0.0f, 0.0f, 0.0f };
        model->dr = model->ds = (Vect){ 0.0f, 0.0f, 0.0f };
    }
    model->mesh = *mesh;
    model->wabble = 0.0f;
}

void vertex_attribute_data (const Model *model, const Renderer *renderer) {
    GL(glUseProgram(renderer->program));
    if (gflag & HAS_VAO)
        GL(glBindVertexArray(model->vao[renderer->program_index]));
    GL(glBindBuffer(GL_ARRAY_BUFFER, model->vbo));
    GL(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model->ebo));
    if (renderer->attribute.snd_edge >= 0) { // if normals will be used
        // vertex attribute
        GL(glEnableVertexAttribArray(renderer->attribute.vertex));
        GL(glVertexAttribPointer(renderer->attribute.vertex, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)0));
        // edges attributes
        GL(glEnableVertexAttribArray(renderer->attribute.fst_edge));
        GL(glVertexAttribPointer(renderer->attribute.fst_edge, 4, GL_SHORT, GL_TRUE, sizeof(Vertex), (void *)sizeof(Quat)));
        GL(glEnableVertexAttribArray(renderer->attribute.snd_edge));
        GL(glVertexAttribPointer(renderer->attribute.snd_edge, 4, GL_SHORT, GL_TRUE, sizeof(Vertex), (void *)(sizeof(Quat)+4*sizeof(int16_t))));
    } else if (renderer->attribute.mapcoord >= 0) { // if texture coordinates will be used
        GL(glEnableVertexAttribArray(renderer->attribute.vertex));
        GL(glVertexAttribPointer(renderer->attribute.vertex, 2, GL_FLOAT, GL_FALSE, sizeof(MapVertex), (void *)0));
        GL(glEnableVertexAttribArray(renderer->attribute.mapcoord));
        GL(glVertexAttribPointer(renderer->attribute.mapcoord, 2, GL_FLOAT, GL_FALSE, sizeof(MapVertex), (void *)(2*sizeof(float))));
    } else {
        // vertex attribute
        GL(glEnableVertexAttribArray(renderer->attribute.vertex));
        GL(glVertexAttribPointer(renderer->attribute.vertex, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)0)); // @DEBUG :: create different type of vertex
    }
    if (gflag & HAS_VAO)
        GL(glBindVertexArray(0));
}

void retrieve_framebuffer (Renderer *renderer) {
    GL(glGenFramebuffers(1, &renderer->fbo));
    GL(glBindFramebuffer(GL_FRAMEBUFFER, renderer->fbo));
    GL(glGenTextures(1, &renderer->framebuffer));
    GL(glBindTexture(GL_TEXTURE_2D, renderer->framebuffer));
    GL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
    GL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
    GL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
    GL(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
    GL(glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, renderer->viewport.w, renderer->viewport.h, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, NULL));
    GL(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, renderer->framebuffer, 0));
#if defined(ANDROID) || defined(IOS) // OpenGLES framebuffers need a color buffer to be complete
    GL(glGenRenderbuffers(1, &renderer->rbo));
    GL(glBindRenderbuffer(GL_RENDERBUFFER, renderer->rbo));
    GL(glRenderbufferStorage(GL_RENDERBUFFER, GL_RGB565, renderer->viewport.w, renderer->viewport.h));
    GL(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, renderer->rbo));
#elif defined(WINDOWS) || defined(LINUX) || defined(MAC) // OpenGL framebuffers don't
    GL(glDrawBuffer(GL_NONE));
    GL(glReadBuffer(GL_NONE));
#endif
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    assertq(status == GL_FRAMEBUFFER_COMPLETE);
    GL(glBindFramebuffer(GL_FRAMEBUFFER, 0));
}

GLuint shader_from_sourcefile (GLenum type, const char *path) {
    GLint nread, status;
    GLuint shader = 0;

    nread = read_file(&gbuffer, SDL_RWFromFile(path, "r"));
    asserth(nread > 0, shader_read_error);
    shader = glCreateShader(type);
    GL(glShaderSource(shader, 1, (const char **)&gbuffer.data, &nread));
    GL(glCompileShader(shader));
    GL(glGetShaderInfoLog(shader, gbuffer.size, &nread, gbuffer.data));
    gbuffer.data[nread] = '\0';
    if (nread > 0)
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "shader compile error", gbuffer.data, NULL);
    GL(glGetShaderiv(shader, GL_COMPILE_STATUS, &status));
    asserth(status == GL_TRUE, shader_compile_error);
    return shader;

shader_compile_error:
    GL(glDeleteShader(shader));
shader_read_error:
    return 0;
}

void draw_models (Renderer *renderer, Camera *camera, GLuint texture, int num_models, Model **model, ...) { // Camera *light_source (for geometry renderer)
    GL(glViewport(renderer->viewport.x, renderer->viewport.y, renderer->viewport.w, renderer->viewport.h));
    GL(glBindFramebuffer(GL_FRAMEBUFFER, renderer->fbo));
    if (renderer->clear_flags)
        GL(glClear(renderer->clear_flags));
    GL(glUseProgram(renderer->program));
    // @implement specific placement of one-time uniforms here
    Camera *light_source = NULL;
    if (renderer->program_index == GEOMETRY_RENDERER) {
        va_list geometry_extras;
        va_start(geometry_extras, model);
        light_source = va_arg(geometry_extras, Camera *);
        va_end(geometry_extras);
        GL(glUniform1i(renderer->uniform[2], 0)); // shadow map
    }
    if (texture) {
        GL(glActiveTexture(GL_TEXTURE0));
        GL(glBindTexture(GL_TEXTURE_2D, texture));
    }
    Mtrx *mtrx_uni = (Mtrx *)(gbuffer.data + 6*sizeof(Mtrx)); // to not collide with mtrx.c
    Vect *vect_uni = (Vect *)(gbuffer.data + 6*sizeof(Mtrx)); // idem
    for (int i = 0; i < num_models; i++) {
        // calculate matrices and send uniforms
        mtrx_uni[0] = *translate(rotm_from_quat(&model[i]->orientation), &model[i]->translation);
        switch (renderer->program_index) {
            case GEOMETRY_RENDERER:
                //printf("WABBLE %d = %.6f\n", i, model[i]->wabble);
                GL(glUniform1f(renderer->uniform[4], model[i]->wabble)); 
                GL(glUniform1f(renderer->uniform[3], model[i]->shininess)); // @merge to make a vector?
                mtrx_uni[1] = *mul_mm(&camera->projm, mul_mm(&camera->viewm, &mtrx_uni[0]));
                mtrx_uni[2] = *rotm_from_quat(&model[i]->orientation);
                mtrx_uni[3] = *mul_mm(&light_source->projm, &light_source->viewm);
                GL(glUniformMatrix4fv(renderer->uniform[0], 4, GL_FALSE, (float *)mtrx_uni));
                // @ continuar com os vetores aqui
                vect_uni[0] = *unit_vect(sub_vv(&light_source->eye, &light_source->aim));
                vect_uni[1] = model[i]->color;
                vect_uni[2] = camera->eye;
                GL(glUniform3fv(renderer->uniform[1], 3, (float *)vect_uni));
                break;
            case SHADOW_RENDERER:
                mtrx_uni[0] = *mul_mm(mul_mm(&camera->projm, &camera->viewm), &mtrx_uni[0]);
                GL(glUniformMatrix4fv(renderer->uniform[0], 1, GL_FALSE, (float *)mtrx_uni));
                break;
            case DEPTHMAP_RENDERER:
                break;
            default: break;
        }
        if (gflag & HAS_VAO) {
            GL(glBindVertexArray(model[i]->vao[renderer->program_index]));
        } else {
            // check if the models don't have the same vbo :: less work
            if (i == 0 || model[i]->vbo != model[i-1]->vbo) {
                GL(glBindBuffer(GL_ARRAY_BUFFER, model[i]->vbo));
                if (renderer->attribute.snd_edge >= 0) { // if normals will be used
                    GL(glVertexAttribPointer(renderer->attribute.vertex, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)0));
                    GL(glVertexAttribPointer(renderer->attribute.fst_edge, 4, GL_SHORT, GL_TRUE, sizeof(Vertex), (void *)sizeof(Quat)));
                    GL(glVertexAttribPointer(renderer->attribute.snd_edge, 4, GL_SHORT, GL_TRUE, sizeof(Vertex), (void *)(sizeof(Quat)+4*sizeof(int16_t))));
                } else if (renderer->attribute.mapcoord >= 0) { // if texture coordinates will be used
                    GL(glVertexAttribPointer(renderer->attribute.vertex, 2, GL_FLOAT, GL_FALSE, sizeof(MapVertex), (void *)0));
                    GL(glVertexAttribPointer(renderer->attribute.mapcoord, 2, GL_FLOAT, GL_FALSE, sizeof(MapVertex), (void *)(2*sizeof(float))));
                } else { // only vertices will be used
                    GL(glVertexAttribPointer(renderer->attribute.vertex, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)0)); // @DEBUG :: create different type of vertex
                }
            } // each model has its index array
            GL(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model[i]->ebo));
        }
        GL(glDrawElements(GL_TRIANGLES, model[i]->mesh.indices.size, GL_UNSIGNED_SHORT, 0));
        // @still need to create different indices arrays for each model and glSubBufferData for each hit
    }
}


void set_opengl_options (void) {
    GL(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));
    GL(glDisable(GL_CULL_FACE));
    GL(glDisable(GL_BLEND));
    GL(glEnable(GL_DEPTH_TEST));
}

