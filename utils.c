#define assertw(expr) if (!(expr)) { report_error(__FILE__, #expr, __LINE__, GL_NO_ERROR); }
#define assertq(expr) if (!(expr)) { report_error(__FILE__, #expr, -__LINE__, GL_NO_ERROR); }
#define asserth(expr, error_handler) if (!(expr)) {\
    report_error(__FILE__, #expr, __LINE__, GL_NO_ERROR);\
    goto error_handler;\
}

#ifdef DEBUG_VERSION
#   define GL(line) do {\
        line;\
        GLint opengl_error = glGetError();\
        if (opengl_error != GL_NO_ERROR)\
            report_error(__FILE__, #line, __LINE__, opengl_error);\
    } while (0)
#else
#   define GL(line) line
#endif

// structs prototypes
typedef struct Buffer_ Buffer;
typedef struct Timer_ Timer;

// function prototypes
void report_error (const char *file_string, const char *line_string, int line_number, int opengl_error);
Buffer alloc_buffer (size_t size, uintptr_t alignment);
Buffer realloc_buffer (Buffer buffer, size_t size, uintptr_t alignment);
size_t read_file (Buffer *buffer, SDL_RWops *file);
size_t write_file (void *buffer, size_t nwrite, SDL_RWops *file);
int printn (const char *format, ...); // from: network.c

struct Buffer_ {
    char *unaligned_ptr;
    char *data;
    size_t size; 
};
struct Timer_ { uint32_t now, last_frame, last_sec, last_reached_server, last_scan, dt, fps; };

enum global_flag {
    QUIT               = 0x01,
    HAS_VAO            = 0x02,
    OPENGL_INITIALIZED = 0x04,
    JOYSTICK_PLUGGED   = 0x08,
    VSYNC_ON           = 0x10,
    SHOW_FPS           = 0x20,
    HAS_TERMINAL       = 0x40,
    FULLSCREEN_ON      = 0x80,
    ALLOW_BROADCAST    = 0x0100,
    ENABLE_PRINTNET    = 0x0200,
    SERVER_ANSWERED    = 0x0400,
    BACKGROUND         = 0x0800,
    RESTORE_SESSION    = 0x1000
};
enum joy_axes { LH, LV, LT, RH, RV, RT };
enum joy_buttons { A, B, X, Y, LB, RB, BACK, START, XBOX, LA, RA };

Buffer gbuffer = { NULL, NULL, 0 };
#if defined(ANDROID) || defined(IOS)
uint32_t gflag = FULLSCREEN_ON; // global flag
#elif defined(LINUX) || defined(WINDOWS) || defined(MAC)
uint32_t gflag = HAS_TERMINAL;
#endif

void report_error (const char *file_string, const char *line_string, int line_number, int opengl_error) {
    size_t size = 435, length = 0;
    char buffer[size]; // to have it's own local buffer is safer
    length = snprintf(buffer, size, "ERROR in file %s, line %d: \"%s\":\n  SDL2: %s\n  errno %d: %s\n",\
        file_string, abs(line_number), line_string, SDL_GetError(), errno, strerror(errno));
    if (gflag & OPENGL_INITIALIZED) {
        length += snprintf(buffer + length, size - length, "  OpenGL: 0x%04x\n",\
            opengl_error ? opengl_error : glGetError());
    }
    buffer[length] = '\0';
    if (gflag & HAS_TERMINAL) {
        fwrite(buffer, sizeof(char), length, stderr);
    } else {
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "ERROR", buffer, NULL);
    }
    if (line_number < 0) {
        exit(line_number);
    }
}

size_t read_file (Buffer *buffer, SDL_RWops *file) {
    char *temp_ptr;
    size_t nread = 0;

    asserth(file != NULL, file_open_error);
    //file_size = SDL_RWsize(file);
    nread = SDL_RWread(file, buffer->data, sizeof(char), buffer->size);
    while (nread == buffer->size) {
        *buffer = realloc_buffer(*buffer, buffer->size * 2, 64);
        asserth(buffer->size != nread, buffer_realloc_error);
        temp_ptr = buffer->data + nread;
        nread += SDL_RWread(file, temp_ptr, sizeof(char), buffer->size/2);
    }
    SDL_RWclose(file);
    buffer->data[nread] = '\0';
    return nread;

buffer_realloc_error:
    SDL_RWclose(file);
file_open_error:
    return 0;
}

size_t write_file (void *buffer, size_t nwrite, SDL_RWops *file) {
    asserth(file != NULL, write_file_open_error);
    nwrite = SDL_RWwrite(file, buffer, nwrite, 1);
    SDL_RWclose(file);
    return nwrite;
write_file_open_error:
    return 0;
}

Buffer alloc_buffer (size_t size, uintptr_t alignment) {
    Buffer buffer;
    alignment--; // alignment must be 2^n
    buffer.unaligned_ptr = (char *)malloc(size);
    assertq(buffer.unaligned_ptr != NULL);
    buffer.data = (char *)(((uintptr_t)buffer.unaligned_ptr + alignment) & ~alignment);
    buffer.size = size - (buffer.data - buffer.unaligned_ptr);
    return buffer;
}

Buffer realloc_buffer (Buffer buffer, size_t new_size, uintptr_t alignment) {
    char *new_memory = (char *)malloc(new_size);
    if (!new_memory) return buffer;
    register char *previous_data = buffer.data;
    buffer.data = (char *)(((uintptr_t)new_memory + alignment) & ~alignment);
    memcpy(buffer.data, previous_data, buffer.size);
    free(buffer.unaligned_ptr);
    buffer.unaligned_ptr = new_memory;
    buffer.size = new_size - (buffer.data - buffer.unaligned_ptr);
    return buffer;
}

void update_timer (Timer *timer) {
    timer->last_frame = timer->now;
    timer->now = SDL_GetTicks();
    timer->dt = timer->now - timer->last_frame;
    timer->fps++;
    if (timer->now - timer->last_sec >= 1000) {
        if (gflag & SHOW_FPS)
            printf("fps = %u\n", timer->fps);
        timer->last_sec = timer->now;
        timer->fps = 0;
    }
}
